#include <iostream>
#include <vector>
#include <string>
using namespace std;

void getMenuDetails();

struct Student
{
	public:
		int studentId;
		string firstName;
		string surName;
		string address;
		int contactNumber;
		int age;
		string courseName;
};

int main()
{
	int choice = 0;
	vector<Student> studentList;
	
	while (true)
	{
		getMenuDetails();
		cin >> choice;
		cout << "\n-----------------------------------------------\n";
		if (choice == 1)
		{
			Student student;
			cout << "Enter Student ID : ";
			cin >> student.studentId;
			cout << "Enter Student First Name : ";
			cin >> student.firstName;
			cout << "Enter Student SurName : ";
			cin >> student.surName;
			cout << "Enter Student Address : ";
			cin >> student.address;
			cout << "Enter Student Contact Number : ";
			cin >> student.contactNumber;
			cout << "Enter Student Course : ";
			cin >> student.courseName;
			cout << "Enter Student Age : ";
			cin >> student.age;

			studentList.push_back(student);
		}
		else
			if (choice == 2)
			{
				int studentId = 0;
				cout << "---------------------\n";
				cout << "Edit Student Details\n";
				cout << "---------------------\n";
				cout << "Enter Student ID : ";
				cin >> studentId;
				cout << "---------------------\n";

				Student searchStudent;

				for (int i = 0; i < studentList.size(); i++)
				{
					searchStudent = studentList.at(i);
					if (searchStudent.studentId == studentId)
					{
						cout << "Enter Student First Name : ";
						cin >> searchStudent.firstName;
						cout << "Enter Student SurName : ";
						cin >> searchStudent.surName;
						cout << "Enter Student Address : ";
						cin >> searchStudent.address;
						cout << "Enter Student Contact Number : ";
						cin >> searchStudent.contactNumber;
						cout << "Enter Student Age : ";
						cin >> searchStudent.age;
						studentList[i] = searchStudent;
						break;
					}
					else
						if(i==studentList.size())
						{
							cout << "No Records Found";
						}
				}

			}
			else
				if (choice == 3)
				{
					cout << "---------------------\n";
					cout << "View Students Details\n";
					cout << "---------------------\n";

					Student viewStudent;

					for (int i = 0; i < studentList.size(); i++)
					{
						viewStudent = studentList.at(i);

						cout << "Student ID : " + to_string(viewStudent.studentId) + " \n";
						cout << "Student First Name : "+ viewStudent.firstName+" \n";
						cout << "Student SurName : "+ viewStudent.surName + " \n";
						cout << "Student Address : "+ viewStudent.address + " \n";
						cout << "Student Contact Number : "+ to_string(viewStudent.contactNumber) + "\n";
						cout << "Student Enrolled Course : "+ viewStudent.courseName+ " \n";
						cout << "Student Age : "+ to_string(viewStudent.age) +" \n";
						cout << "---------------------------------\n";

					}
				}
				else
					if (choice == 4)
					{
						int studentId = 0;
						cout << "---------------------\n";
						cout << "Search Student Details\n";
						cout << "---------------------\n";
						cout << "Enter Student ID : ";
						cin >> studentId;
						cout << "---------------------\n";

						Student searchStudent;

						for (int i = 0; i < studentList.size(); i++)
						{
							searchStudent = studentList.at(i);
							if (searchStudent.studentId == studentId)
							{
								cout << "Student ID : " + to_string(searchStudent.studentId) + " \n";
								cout << "Student First Name : " + searchStudent.firstName + " \n";
								cout << "Student SurName : " + searchStudent.surName + " \n";
								cout << "Student Address : " + searchStudent.address + " \n";
								cout << "Student Contact Number : " + to_string(searchStudent.contactNumber) + " \n";
								cout << "Student Enrolled Course : " + searchStudent.courseName + " \n";
								cout << "Student Age : " + to_string(searchStudent.age) +" "+ " \n";
								cout << "---------------------------------\n";
								break;
							}
							else 
								if (i == studentList.size() - 1)
								{
									cout << "No Records Found";
								}
						}
					}
					else
						if (choice == 5)
						{
							string courseName = "";
							cout << "---------------------\n";
							cout << "Search Student List\n";
							cout << "---------------------\n";
							cout << "Enter Course Name : ";
							cin >> courseName;
							cout << "---------------------\n";

							Student searchStudent;
							vector<Student> courseStudentList;
							Student courseEnrollStudent;

							for (int i = 0; i < studentList.size(); i++)
							{
								searchStudent = studentList.at(i);
								if (searchStudent.courseName.compare(courseName)==0)
								{
									courseStudentList.push_back(searchStudent);
								}
							}

							for (int i = 0; i < courseStudentList.size(); i++)
							{
								courseEnrollStudent = courseStudentList.at(i);

								cout << "Student ID : " + to_string(courseEnrollStudent.studentId) + " \n";
								cout << "Student First Name : " + courseEnrollStudent.firstName + " \n";
								cout << "Student SurName : " + courseEnrollStudent.surName + " \n";
								cout << "Student Address : " + courseEnrollStudent.address + " \n";
								cout << "Student Contact Number : " + to_string(courseEnrollStudent.contactNumber) + " \n";
								cout << "Student Enrolled Course : " + courseEnrollStudent.courseName + " \n";
								cout << "Student Age : " + to_string(courseEnrollStudent.age) + " \n";
								cout << "---------------------------------\n";
							}
						}

						else
							if(choice==6)
							{
								int studentId = 0;
								cout << "---------------------\n";
								cout << "Delete Student Details\n";
								cout << "---------------------\n";
								cout << "Enter Student ID : ";
								cin >> studentId;

								Student searchStudent;

								for (int i = 0; i < studentList.size(); i++)
								{
									searchStudent = studentList.at(i);
									if (searchStudent.studentId == studentId)
									{
										studentList.erase(studentList.begin() + i);
										break;
									}
									else
										if (i == studentList.size() - 1)
										{
											cout << "No Records Found";
										}
								}
							}
							else
								if (choice == 7)
								{
									break;
								}


	}

	return 0;
}

void getMenuDetails()
{
	cout << "-----------------------------------------------\n";
	cout << "Welcome to University Student Management System\n";
	cout << "-----------------------------------------------\n";
	cout << "1. Add Student Details\n";
	cout << "2. Edit Student Details\n";
	cout << "3. View Students Details\n";
	cout << "4. Search Student Details\n";
	cout << "5. Search Student List\n";
	cout << "6. Delete Student Record\n";
	cout << "7. Exit\n";
	cout << "-----------------------------------------------\n";
	cout << "   Enter Your Choice : ";
};
